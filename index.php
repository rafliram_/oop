<?php 
require_once ('animal.php');

$sheep = new animal('shaun');

echo $sheep->name;
echo "<br>";
echo $sheep->legs;
echo "<br>";
echo $sheep->cold_blooded;

echo "<br>";
require_once ('ape.php');
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo "<br>";
require_once ('frog.php');
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
 ?>